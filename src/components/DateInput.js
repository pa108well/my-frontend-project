import React, { Component } from 'react';

const DateInput = (props) => {

  return(
    <label>
      {props.label}
      <input type="number" {...props} />
    </label>
  )
}

export default DateInput;
