import React, { Component } from 'react';
import styles from './Loader.module.css';

console.log(styles);

const Loader = () => {

  return(
    <div className={styles.loader}>Loading</div>
  )
}

export default Loader;
