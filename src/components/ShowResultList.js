import React, { Component } from 'react';

const ShowResultList = (props) => {

  const items = props.list.map((item, key) =>
    <tr key={key.toString()}>
      <td>{item.year}</td>
      <td>{item.mockResponse.l}</td>
      <td>{item.mockResponse.s}</td>
      <td>{item.mockResponse.h}</td>
      <td>{item.mockResponse.e}</td>
    </tr>
);

  return(
    <div className="result-block">
      {props.resultStatus === 'success' &&
        <table>
            <thead>
              <tr>
                <th>Year:</th>
                <th>Low:</th>
                <th>S:</th>
                <th>High:</th>
                <th>E:</th>
              </tr>
            </thead>
          <tbody>
            {items}
            <tr>
             <th></th>
             <th>{"lowest: " + props.highLow.low}</th>
             <th></th>
             <th>{"highest: " + props.highLow.high}</th>
            </tr>
          </tbody>

        </table>

      }

      {props.resultStatus === 'failed' &&
        <p>Failed</p>
      }
    </div>
  )
}

export default ShowResultList;
