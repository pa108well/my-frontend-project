import React, { Component } from 'react';
import DateInput from './components/DateInput.js'
import axios from 'axios';
import Loader from './components/Loader.js'
import ShowResultList from './components/ShowResultList.js'
import './App.css';

let jsonpAdapter = require('axios-jsonp');

const statusText = {
  success: 'success',
  failed: 'failed'
}

const defaultDates = {
  from: 2010,
  to: 2019
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showingResult: false,
      resultStatus: undefined,
      loading: false,
      form: {
        from: defaultDates.from,
        to: defaultDates.to
      },
      resultList: {},
      minMax: {}
    };
    this.handleSubmitForm=this.handleSubmitForm.bind(this);
  }

  handleSubmitForm(e){

    e.preventDefault();

    this.setState({
      loading: true,
      showingResult: false
    });


    axios.get(`http://localhost:35000/`, {
      withCredentials: true,
      crossdomain: true,
      params: this.state.form,
      headers: {
    	  'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/javascript',
        'Accept': 'application/javascript'
    	},
      adapter: jsonpAdapter
    })
      .then(res => {
        this.setState({
          resultStatus: statusText.success,
          resultList: res && res.data && res.data.ohlc || [],
          minMax: res && res.data && res.data.minMax
        });
        console.log('resultList', this.state.resultList);
      }).catch((error) => {
        this.setState({
          resultStatus: statusText.failed,
          resultList: {},
          minMax: {}
        });
      }).finally(()=>{
        this.setState({
          loading: false,
          showingResult: true
        });
      });

  }

  changeForm = (target, event) => {

    let value;
    if(target==='from'){
      value = {
        from: event.target.value || defaultDates.from,
        to: this.state.form.to || defaultDates.to
      }
    }
    else if(target==='to'){
      value = {
        to: event.target.value || defaultDates.to,
        from: this.state.form.from || defaultDates.from
      }
    }

    this.setState({
      form: value
    });
  }

  render() {

    const { form, loading, resultList, resultStatus, minMax } = this.state;
    const formIsFilledCorrect = form.from >= defaultDates.from && form.to <= defaultDates.to && form.to >= form.from;

    return (
      <div className="App">
        <form onSubmit={this.handleSubmitForm} >
          <DateInput label="From" name="from" value={form.from} onChange={(e) => this.changeForm('from', e)} />
          <DateInput label="To" name="to" value={form.to} onChange={(e) => this.changeForm('to', e)} />
          <input disabled={!formIsFilledCorrect || loading} type="submit" />
        </form>
        {loading && <Loader />}

        {this.state.showingResult && <ShowResultList resultStatus={resultStatus} highLow={minMax} list={resultList} />}

      </div>
    );
  }
}

export default App;
