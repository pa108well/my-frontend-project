var express = require('express');
var cors = require('cors')
var app = express();
app.use(cors())

let minLow;
let maxHigh;

app.get('/', function (req, res) {
  minLow =  Number.MAX_VALUE;
  maxHigh = Number.MIN_VALUE;
  var data = {
    "result": "OK",
    "ohlc": createMockResponse(req.query.from, req.query.to),
    "minMax" : { "high": maxHigh, "low": minLow}
  };
  setTimeout(function(){
    res.jsonp(data);
  }, 2000);

});


function createMockResponse(from, to) {
  var ohlcList = [];
  let years = to - from;
    for(i = 0; i <= years; i++) {
      let ohlc = { "year":to-i, "mockResponse":{ "l":Math.random(),"h":Math.random(), "s":Math.random(), "e":Math.random()}}
      ohlcList.push(ohlc);
        if(ohlc.mockResponse.l < minLow) {
          minLow = ohlc.mockResponse.l
        } if(ohlc.mockResponse.h > maxHigh) {
            maxHigh = ohlc.mockResponse.h
          }
    }
  return ohlcList;
}



app.listen(35000, function () {
  console.log('Server app listening on port 35000!');
});
